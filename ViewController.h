//
//  ViewController.m
//  手势解锁2
//
//  Created by kong on 15/5/7.
//  Copyright (c) 2015年 LKWeiLiao. All rights reserved.
//

#import "ViewController.h"
#import "UIView+Extension.h"
#import "LKPaintView.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[UIColor grayColor]];
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    
    LKPaintView *pv = [[LKPaintView alloc]init];
    pv.backgroundColor = [UIColor grayColor];
    pv.frame =CGRectMake(0, self.view.height * 0.4, width, self.view.height * 0.6);

    [self.view addSubview:pv];
    
}

@end
